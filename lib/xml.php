<?php
/**
* Xml abstraction layer
*
* This file has the core class to deal with xmlfiles
* TODO: Fix this to to something good :P
*
* PHP versions 4 and 5
*
* LICENSE: TODO
*
* @category   XML Library
* @package    FallenLib
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    1.0
* @link       http://fallen.trans-center.org/lib/php
* @deprecated File deprecated in Release 0.1
*/

// {{{ XmlData

/**
* Class that act as a layer to acess SIMPLE xml files
*
* TODO: fix this class to do some realwork
*
* @category   Database Library
* @package    FallenLib
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    Release: 1.0.0
* @link       http://fallen.trans-center.org/lib/php
* @deprecated Class deprecated in Release 0.0
*/
class XmlData
{
   /**
	 * Parser selector
    *
    * Actual parser set
    *
    * @var ??
   */
   var $parser;
   var $data;
   var $flag;
   var $root;
   var $stack;
	
	// {{{ xmlData()

	/**
    * Class constructor
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function XmlData()
   {
      $this->parser = Null;
      $this->flag = False;
      $this->stack = array();
   }
	
	// }}}
	// {{{ _open()

	/**
    * Sets parser object
	 *
    * @return void
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
   function _open()
   {
      if($this->parser):
			$this->close();
      else:
         $this->parser = xml_parser_create();
         xml_set_object($this->parser, $this);
         //config
         xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, False);
         xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, True);
         //set data handlers
         xml_set_element_handler($this->parser, "parser_start", "parser_end");
         xml_set_character_data_handler($this->parser, "parser_data");
      endif;
   }

	// }}}
	// {{{ _close()

	/**
    * Free parser
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function _close()
   {
      if($this->parser):
         xml_parser_free($this->parser);
         $this->parser = Null;
      else:
         print("<p>Cannot close file. It isn't opened.</p>");
      endif;
   }
	
	// }}}
	// {{{ read()

	/**
    * Read and parse file data from a xml file
    *
	 * @param string $name  xml filename
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function read($name)
   {
      //open xml
      $this->_open();

      //process it
      $fdata = file($name);
      foreach($fdata as $line)
         xml_parse($this->parser, $line);

      //close it
      $this->_close();
   }

	// }}}
	// {{{ parser_start()

	/**
    * Generic method to parse an open tag
    *
	 * @param string $parser  parser link
	 * @param string $element  element name
	 * @param array $attr  attribute list
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function parser_start($parser, $element, $attr)
   {
      if($this->flag)
         array_push($this->stack, $element);
      else
         $this->flag = True;
   }
	
	// }}}
	// {{{ parser_end()
  
	/**
    * Generic method to parse an close tag
    *
	 * @param string $parser  parser link
	 * @param string $element  element name
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function parser_end($parser, $element)
   {
      array_pop($this->stack);
   }
	
	// }}}
	// {{{ parser_data()

	/**
    * Generic method to parse data from tags
    *
	 * @param string $parser  parser link
	 * @param string $cdata  tag data
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function parser_data($parser, $cdata)
   {
      $index = count($this->stack) - 1;
      if($cdata{0} != "\n")
         $this->data[$this->root][$this->stack[$index]] = $cdata;
      elseif($index + 1)
         $this->root = $this->stack[$index];
   }
	
	// }}}
	
}

// }}}
?>