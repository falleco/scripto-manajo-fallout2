<?php
/**
* MySql abstraction layer
*
* This file has the core class to deal with database
*
* PHP versions 4 and 5
*
* LICENSE: TODO
*
* @category   Database Library
* @package    FallenLib
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    1.0
* @link       http://fallen.trans-center.org/lib/php
* @deprecated File deprecated in Release 0.1
*/

// {{{ constants

/**
 * Option to make query normal with commas
 */
define('SQL_NORMAL',  0);

/**
 * Option to make query logic with AND
 */
define('SQL_AND',  1);

/**
 * Option to make query logic with OR
 */
define('SQL_OR',  2);

// }}}
// {{{ MySQLData

/**
* Class that act as a layer to acess mysqldata
*
* Long description for class (if any)...
*
* @category   Database Library
* @package    FallenLib
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    Release: 1.0.0
* @link       http://fallen.trans-center.org/lib/php
* @deprecated Class deprecated in Release 0.0
*/

class MySQLData
{
   /**
	 * Mysql connection
    *
    * Link to the actual connection
    *
    * @var MySql Resource
   */
   var $_link;
	
	/**
	 * Database prefix
    *
    * Prefix to append to every table in database
    *
    * @var MySql Resource
   */
   var $_prefix;
   
	// {{{ MySQLData()
	
	/**
    * Class constructor
    *
	 * @param string $prefix  table prefix
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function MySQLData($prefix)
   {
		$this->_link = Null;
		$this->setPrefix($prefix);
   }
	
	// }}}
	// {{{ connect()
    
	/**
    * Connect to the host
    *
	 * @param string $user  database user
	 * @param string $pass  database password
	 * @param string $db  database to connect (optional)
	 * @param string $host  host to connect
	 *
    * @return bool True if connect, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function connect($user, $pass, $db=Null, $host="localhost")
   {
		$this->_link = mysql_connect($host, $user, $pass);
		if(!$this->_link):
			print("<p>ERROR: Cannot connect to the database.</p>\n");
			return False;
		else:
			if($db)
				$this->setDataBase($db, True);
			return True;
		endif;
   }
	
	// }}}
	// {{{ close()
	
	/**
    * Close actual connection
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function close()
   {
		mysql_close($this->_link);
   }
	
	// }}}
	// {{{ setDataBase

	/**
    * Select database
    *
	 * @param string $db  database name
	 * @param bool $die  kills the script if set
	 *
    * @return bool True if selected, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function setDataBase($db, $die=False)
   {
		if(! mysql_select_db($db, $this->_link)):
			if($die)
				die("<p>Cannot chose given database.</p>");
			else
				return False;
		else:
				return True;
		endif;
   }
	
	// }}}
	// {{{ setPrefix()

	/**
    * Set database prefix
    *
	 * @param string $prefix  prefix to apply
	 *
    * @return bool True if selected, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function setPrefix($prefix)
   {
		$this->_prefix = $prefix;
   }
   
	// }}}
	// {{{ _makeQuery()

	/**
    * Create a valid string to use with a query
    *
	 * @param array $data  aray to use
	 * @param int $cond  user , OR or AND
	 *
    * @return string with the query
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
   function _makeQuery($data, $cond=SQL_NORMAL)
   {
		$values = array();
		foreach($data as $k => $v)
			$values[] = "{$k}='{$v}'";
		
		//if($cond = SQL_AND)
		if($cond)
			return implode(' AND ', $values);
		//elseif($cond = SQL_OR)
		//	return implode(' OR ', $values);
		else
			return implode(',', $values);
   }
	
	// }}}
	// {{{ readData()
	
	/**
    * Read data from a table
    *
	 * @param string $table  table to read
	 * @param array $cond  conditions
	 * @param bool $force  force the return of one unique entry
	 * @param int $cond  use , OR or AND
	 * @param string $rows  misc option
	 * @param string $misc	some other thing needed in the query
	 *
    * @return array  with results
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function readData($table, $cond=Null, $force=False, $logic=SQL_NORMAL, $rows="*", $misc="")
   {
		$query = "SELECT {$rows} FROM {$this->_prefix}{$table}";
		if($cond)	//append conditions if there's any
			 $query .= " WHERE ".$this->_makeQuery($cond, $logic);

		$query .= $misc; //append misc things to the query
		
		$result = mysql_query($query, $this->_link);
		if(!$result)
			 die("<p>Cannot read data from database.<br />Why?<br />".mysql_error()."</p>");
		
		if(mysql_num_rows($result) == 0):
			return 0;
		elseif($force):
			$main = mysql_fetch_array($result);
			if(mysql_num_rows($result) == 1)
				return $main;
			else	//returns first ocurrency
				return $main[0];
		else:
			$main = array();
			while($p = mysql_fetch_array($result))
				$main[] = $p;
		endif;
		
		return $main;
   }
	
	// }}}
	// {{{ addData()

	/**
    * Add data to table
    *
	 * @param string $table  table to read
	 * @param array $data  data to add
	 *
    * @return bool  True if ok, otherwise returns false
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function addData($table, $data)
   {
		$result = mysql_query("INSERT INTO {$this->_prefix}{$table} SET ".$this->_makeQuery($data), $this->_link);
		if(!$result):
			echo "<p>Cannot insert data into database.<br />Why?<br />".mysql_error()."</p>";
			return False;
		else:
			return True;
		endif;
   }

	// }}}
	// {{{ delData()
	
	/**
    * Removes data from table
    *
	 * @param string $table  table to read
	 * @param array $cond  conditions
	 *
    * @return bool True if ok, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function delData($table, $cond=Null)
   {
		$query = "DELETE FROM {$this->_prefix}{$table}";
		if($cond)
			$query .= " WHERE ".$this->_makeQuery($cond);

		$result = mysql_query($query, $this->_link);
		if(!$result):
			echo "<p>Cannot remove data from database.<br />Why?<br />".mysql_error()."</p>";
			return False;
		else:
			return True;
		endif;
   }
	
	// }}}
	// {{{ setData

	/**
    * Edit data
    *
	 * @param string $table  table to read
	 * @param array $data  data to edit
	 * @param array $cond  conditions
	 *
    * @return bool True if ok, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function setData($table, $data, $cond=Null)
   {
		$query = "UPDATE {$this->_prefix}{$table} SET ".$this->_makeQuery($data);
		if($cond)
			 $query .= " WHERE ".$this->_makeQuery($cond);

		$result = mysql_query($query, $this->_link);
		if(!$result):
			echo "<p>Cannot edit data.<br />Why?<br />{mysql_error()}</p>";
			return False;
		else:
			return True;
		endif;
   }
	
	// }}}
	// {{{ query()

	/**
    * Makes a raw query to the database
    *
	 * @param string $query  user suplied query
	 *
    * @return MySQL Resource  raw results, return False if query failed
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function query($query)
   {
		$result = mysql_query($query, $this->_link);
		if(!$result):
			echo "<p>Cannot execute database query.<br />Why?<br />".mysql_error()."</p>";
			return False;
		else:
			return $result;
		endif;
   }
	
	// }}}
	
}

// }}}
?>