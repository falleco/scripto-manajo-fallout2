<?php
  require_once("./ScriptoManajo.php");
  $script = new Script("./config.xml");
  //--------------------------------------------------------------------------//
	if(!isset($_REQUEST["logout"])):
		if(!$_REQUEST["user"] or !$_REQUEST["pass"])
			header("Location: ./index.php?page=".MSG_EMPTYLOGIN);
		elseif($script->login($_REQUEST["user"], $_REQUEST["pass"]))
				header("Location: ./index.php");
		else
			header("Location: ./index.php?page=".MSG_WRONGLOGIN);
	else:
		$script->logout();
		header("Location: ./index.php");
	endif;
?> 
