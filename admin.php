<?php
   include_once("./ScriptoManajo.php");
	$script = new Script("./config.xml");
	if(!$script->isLoged())
		die('Acesso negado: � preciso logar pela p�gina principal.<meta http-equiv="refresh" content="2;url=./index.php">');
	elseif($script->userGetLevel() == LEVEL_OFF)
		die('Acesso negado: Seu status atual � de usu�rio inativo, entre em contato com algum administrador caso queira alterar isto.<meta http-equiv="refresh" content="2;url=./index.php">');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt_BR">
    <head>    
        <title>Scripto Manajo - Administra��o</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" type="text/css" href="./main.css" />
		  <link rel="shortcut icon" href="./imgs/admin.png" type="image/x-icon" />
	<!--[if lt IE 7]>
		<script defer type="text/javascript" src="./data/pngfix.js"></script>
			<script type="text/javascript" src="./data/menufix.js"></script>
			<link rel="stylesheet" type="text/css" media="screen" href="ie.css" />
	<![endif]-->
    </head>
    <body>
        <!-- XXXXXXX TOPO DA P�GINA XXXXXXX-->
        <div id="header">       
            <span>[Administra��o]</span>
        </div>
    
        <!-- XXXXXXX BARRA DE MENU/LOGIN XXXXXXX-->
        <ul id="menu">
            <li><a href="#help">Informa��es</a></li>
            <li><a href="#AddMember">Adicionar Membro</a></li>
            <li><a href="#EditMember">Editar Membros</a></li>       
            <li><a href="#Locks">Trancas</a></li>
            <li><a href="#SendScript">Enviar Script</a></li>
            <li><a href="#EditScript">Editar Scripts</a></li>
            <li><a href="./index.php">Sair</a></li>
        </ul>

        <div id="main">
		  <?php
				if(isset($_REQUEST["page"]))
				{
					// --- Deal with locks
					if($_REQUEST["page"] == OPT_LOCK)
					{
						if($_REQUEST["lock"] == "on"):
							echo '<h2>Trancando Pasta '.basename($_REQUEST["dirName"]).': ';
							$script->lock($_REQUEST["dirName"]);
						else:
							echo '<h2>Destrancando Pasta '.basename($_REQUEST["dirName"]).': ';
							$script->unlock($_REQUEST["dirName"]);
						endif;
						echo "<strong>Pronto</strong></h2>";
					}
					// -- Dreal with Scripts
					elseif($_REQUEST["page"] == OPT_SCRIPT)
					{
						// Delete a script
						if($_REQUEST["opt"] == "del"):
							echo '<h2>Removendo informa��es do script: ';
							$script->scriptDel($_REQUEST["id"]);
							echo "<strong>Pronto</strong></h2>";
						
						// Show form to edit a script
						elseif($_REQUEST["opt"] == "form"):
							$info = $script->scriptGet($_REQUEST["id"]);
							echo '<h2>Editar script: '.$info["file"].'</h2>';
							echo '<table class="users">
							  <tr>
									  <td class="file">Arquivo</td>
									  <td class="nick">Tradutor</td>
									  <td class="file">Vers�o</td>
									  <td class="file">Revis�o</td>
									  <td class="comments">Coment�rio</td>
									  <td class="file">Status</td>
									  <td class="misc">A��es</td>
							</tr><tr>';
							echo '<form action="'.$base.'?page='.OPT_SCRIPT.'&opt=edit'.'" method="POST">';
							echo '<td>'.$info["file"].'</td>';
							echo '<td>'.$script->userGetName($info["tid"]).'</td>';
							echo '<td><input name="version" class="form" type="text" size="5" maxlen="5" value="'.$info["version"].'"/></td>';
							//show revision options
							echo '<td><select name="rev">';
							if($info["rev"] == "1"):
								echo '<option value="1">Sim</option>';
								echo '<option value="0">N�o</option>';
							else:
								echo '<option value="0">N�o</option>';
								echo '<option value="1">Sim</option>';
							endif;
                     echo '</select></td>';
							//echo '<td><input name="rev" class="form" type="text" size="1" maxlen="1" value="'.$info["rev"].'"/></td>';
							echo '<td><input name="comment" class="form" type="text" size="30" maxlen="255" value="'.$info["comment"].'"/></td>';
							//show status info
							echo '<td><select name="status">';
							$opts = array(STATUS_LOCK => "Trancado", STATUS_INVALID => "Inv�lido", STATUS_GOT => "Traduzindo", STATUS_OK => "Traduzido");
							
							
							echo '<option value="'.$info["status"].'">'.$opts[$info["status"]].'</option>';
							unset($opts[$info["status"]]);

							foreach($opts as $on => $ov)
								echo '<option value="'.$on.'">'.$ov.'</option>';
                     echo '</select></td>';
							//echo '<td><input name="status" class="form" type="text" size="1" maxlen="1" value="'.$info["status"].'"/></td>';
							echo '<td><input type="hidden" name="id" value="'.$_REQUEST["id"].'"/><input class="button" type="submit" value="Alterar"/></td>';
							echo '</form>';
							echo '</tr></table>';
						
						// insert edited info into database
						else:
							echo '<h2>Editando informa��es do script: ';
							$script->scriptEdit($_REQUEST["id"], $_REQUEST["version"], $_REQUEST["rev"], $_REQUEST["comment"], $_REQUEST["status"]);
							echo "<strong>Pronto</strong></h2>";
						endif;
					}
					//-- Deal with users
					elseif($_REQUEST["page"] == OPT_USER)
					{
						//Remove users
						if($_REQUEST["opt"] == "del"):
							echo '<h2>Removendo usu�rio: ';
							$script->userDel($_REQUEST["id"]);
							echo "<strong>Pronto</strong></h2>";
							
						elseif($_REQUEST["opt"] == "level"):
							$usr = $script->userGet($_REQUEST["id"]);
							echo '<form action="'.$base.'?page='.OPT_USER.'" method="POST">';
							echo '<h2>Editar n�vel de: '.$script->userGetName($_REQUEST["id"]);							
							//show levels							
							echo ' <select name="level">';
							$opts = array(LEVEL_OFF => "Inativo", LEVEL_TRS => "Tradutor", LEVEL_REV => "Revisor", LEVEL_BTH => "Trad&Rev", LEVEL_ADM => "Administrador");
							
							echo '<option value="'.$usr["level"].'">'.$opts[$usr["level"]].'</option>';
							unset($opts[$user["level"]]);

							foreach($opts as $on => $ov)
								echo '<option value="'.$on.'">'.$ov.'</option>';
                     echo '</select>';
							echo '<input name="opt" type="hidden" value="editLevel">';
							echo '<input name="id" type="hidden" value="'.$_REQUEST["id"].'">';
							echo ' <input class="button" type="submit" value="Alterar">';
							echo '</h2></form>';
							
						elseif($_REQUEST["opt"] == "editLevel"):
							echo '<h2>Alterando n�vel de usu�rio: ';
							$usr = $script->userSetLevel($_REQUEST["id"], $_REQUEST["level"]);
							echo "<strong>Pronto</strong></h2>";
							
						//Add user
						elseif($_REQUEST["opt"] == "add"):
							echo '<h2>Adicionando usu�rio: ';
							$script->userAdd($_REQUEST["nick"], $_REQUEST["pass"], $_REQUEST["email"], $_REQUEST["level"]);
							echo "<strong>Pronto</strong></h2>";
						
						//Edit user
						else:
							echo '<h2>Editando informa��es do usu�rio: ';
							if(isset($_REQUEST["pass"]))
								$script->userEdit($_REQUEST["id"], $_REQUEST["nick"], $_REQUEST["email"], $_REQUEST["pass"]);
							else
								$script->userEdit($_REQUEST["id"], $_REQUEST["nick"], $_REQUEST["email"]);
							echo "<strong>Pronto</strong></h2>";
						endif;
						
						
					}
				
				} 
				
				if($script->userGetLevel() == LEVEL_ADM):
					echo '<h2 id="help">Administra��o:</h2>
					<div class="note">
						 
						 <h3>Organiza��o geral</h3>
						 <p>Os dados est�o na seguinte ordem:
						 <pre class="center">Nome Do Arquivo - Respons�vel Pela tradu��o - Vers�o - Revisado? - Coment�rio - Status </pre></p>
						 
						 <h3>Os arquivos</h3>
						 <p>O nome do arquivo n�o � edit�vel. No Status vc pode por qualquer coisa(at� 10 caracteres) para algum controle pessoal, mas somente os arquivos que tiverem seguintes status ser�o exibidos:</p>
						 <ul>
							  <li><strong>\'livre\'</strong> - Scripts com o nome PRETO, significando que est�o livres para tradu��o.</li>
							  <li><strong>\'fechado\'</strong> - Scripts com o nome em VERMELHO, e impossibilitados para a tradu��o.</li>
							  <li><strong>\'andamento\'</strong> - Scripts com o nome em VERDE, j� foram p�gos por algum tradutor, mas ainda n�o foram entregues, ou enviados para o servidor.</li>
							  <li><strong>\'traduzido\'</strong> - Scripts com o nome AZUL, j� pegos e traduzidos por algu�m.</li>
							  <li><strong>\'pronto\'</strong> - Scripts com o nome em ROXO, poss�velmente estar�o PRONTOS para a inser��o.</li>
						 </ul>
	
						 <h3>N�veis de usu�rios:</h3>
						 <ul>
							  <li>0: Membro inativo, n�o conseguir� logar.</li>
							  <li>1 - 3: Tradutor.</li>
							  <li>4 - 6: Revisor.</li>
							  <li>7 - 9: Tradutor E revisor.</li>
							  <li>10: Administrador.</li>
						 </ul>
					</div>';
				
					echo '<!-- Lista os membros -->';
					echo '<h2 id="AddMember">Adicionar Membro</h2>';
					echo '<form action="'.$base."?page=".OPT_USER.'" method="POST">';
					echo '<table class="users">
						 <tr>
							  <td class="nick">Nick: </td><td><input class="form" name="nick" type="text" value="Nick"></td>
						 </tr>
						 <tr>
							  <td class="nick">Senha: </td><td><input class="form" name="pass" type="text" value="Senha"></td>
						 </tr>
						 <tr>
							  <td class="nick">Email: </td><td><input class="form" name="email" type="text" value="user@noplace.org"></td>
						 </tr>
						 <tr>
							  <td class="level">N�vel: </td><td>
							  <select name="level">
							  <option class="form" value="<?php echo LEVEL_OFF ?>">Inativo</option>
							  <option class="form" value="<?php echo LEVEL_TRS ?>">Tradutor</option>
							  <option class="form" value="<?php echo LEVEL_REV ?>">Revisor</option>
							  <option class="form" value="<?php echo LEVEL_BTH ?>">Tradutor E Revisor</option>
							  <option class="form" value="<?php echo LEVEL_ADM ?>">Administrador</option>
							  </select>
							  <input name="opt" type="hidden" value="add">
							  </td>
						 </tr>
					</table>
					<input class="button" type="submit" value="Adicionar Membro">
					</form>';
				
					echo '<!-- Lista os membros -->';
					echo '<h2 id="EditMember">Editar Membros</h2>';
					echo '<div class="note"><h3>Dica: Deixe o campo de Senha em branco para manter a senha atual.</h3></div>';
					$script->userList();
				
					echo '<!-- Pastas trancadas -->';
					echo '<h2 id="Locks">Edi��o de pastas</h2>';
					$script->listLock();
				endif;
				?>

            <!-- Enviar arquivos 
            <h2 id="SendScript">Enviar Scripts Traduzidos</h2>
            <p class="center"> Todos os scripts ser�o enviados para uma pasta para aguardar ser
            liberado por um administrador.
            <form enctype="multipart/form-data" method="post" name="sendScript">
                <input type="hidden" name="opt" value="send">
                <input name="userfile" type="file"> <input type="submit" class="button" value="Enviar">
            </form>
            </p> -->

				<?php
				if(($script->userGetLevel() == LEVEL_ADM) or ($script->userGetLevel() == LEVEL_REV) or ($script->userGetLevel() == LEVEL_BTH)):
					echo '<!-- Edi��o de scripts -->';
					echo '<h2 id="EditScript">Edi��o de Scripts</h2>';
					$script->admin_ListDirs();
				else:
					echo "<p>Voc� n�o deveria estar aqui.</p>";
				endif;
				?>

        </div> 
    </body>
</html>
