<?php
/**
* ScriptoManajo Core
*
* This file has the core classes and functions used in ScriptoManajo system
*
* PHP versions 4 and 5
*
* LICENSE: TODO
*
* @category   Scripts Manager
* @package    ScriptoManajo
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    2.0b
* @link       http://fallen.trans-center.org/scripto
* @since      File available since Release 1.0
* @deprecated File deprecated in Release 1.0
*/

// {{{ files

include_once("./lib/xml.php");
include_once("./lib/mysql.php");

// }}}
// {{{ constants

/**
 * Menu Option that calls login form
 */
define("MENU_LOGN", md5(md5("loginForm")));

/**
 * Menu Option that calls info page
 */
define("MENU_INFO", md5(md5("infopage")));


/**
 * Menu Option that calls project information section
 */
define("MENU_PROJ", md5(md5("about")));

/**
 * Menu Option that calls ScriptoManajo information section
 */
define("MENU_ABOT", md5(md5("about")));

/**
 * Menu Option that calls contact page
 */
define("MENU_CONT", md5(md5("cont")));

/**
 * Used to identify not-implemented pages
 */
define("MENU_OFF",  md5(md5("off")));

/**
 * Option to a user get a script
 */
define('OPT_GET', md5(md5(1)));

/**
 * Option to remove a script from user
 */
define('OPT_DEL', md5(md5(2)));

/**
 * Option to add users/scripts
 */
define('OPT_ADD', md5(md5(3)));

/**
 * Lock directories option
 */
define('OPT_LOCK', md5(md5(5)));

/**
 * Users related options
 */
define('OPT_USER', md5(md5(10)));

/**
 * Scripts related options
 */
define('OPT_SCRIPT', md5(md5(15)));

/**
 * Users level: Banned
 */
define('LEVEL_OFF', 0);

/**
 * Users level: Translator
 */
define('LEVEL_TRS', 5);

/**
 * Users level: Reviewer
 */
define('LEVEL_REV', 10);

/**
 * Users level: Translator & Reviewer
 */
define('LEVEL_BTH', 15);

/**
 * Users level: Administrator
 */
define('LEVEL_ADM', 20);

/**
 * Script Status: Locked
 */
define('STATUS_LOCK', 0);

/**
 * Script Status: Invalid
 */
define('STATUS_INVALID', 6);

/**
 * Script Status: Free to translate
 */
//define('STATUS_FREE', 1);

/**
 * Script Status: Already owned by any translator
 */
define('STATUS_GOT', 2);

/**
 * Script Status: Translated but not yet revised
 */
define('STATUS_OK', 3);

/**
 * Script Status: Translated AND revised
 */
//define('STATUS_DONE', 5);

/**
 * Message id: Login is needed
 */
define('MSG_NEEDLOGIN',  md5(md5(20)));

/**
 * Message id: User didn't suply all information needed
 */
define('MSG_EMPTYLOGIN',  md5(md5(10)));

/**
 * Message id: User suplied wrong information
 */
define('MSG_WRONGLOGIN',  md5(md5(12)));

/**
 * Users database name
 */
define('DB_USERS',  "users");

/**
 * Scripts database name
 */
define('DB_SCRIPTS',  "scripts");

// }}}
// {{{ GLOBALS

/**
 * Base url to the actual page protected
 * @global string $GLOBALS['base']
 */
$GLOBALS['base'] = htmlentities($_SERVER['PHP_SELF']);

// }}}
// {{{ Script

/**
* Class that acta as core of ScriptoManajo
*
* Long description for class (if any)...
*
* @category   Scripts Manager
* @package    ScriptoManajo
* @author     Israel G. Crisanto <israel.crisanto@gmail.com>
* @copyright  2006 The Trans-Center Group
* @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
* @version    Release: 1.0.0
* @link       http://pear.php.net/package/PackageName
* @deprecated Class deprecated in Release 0.0.0
*/
class Script
{
   // {{{ properties

   /**
	 * Options of ScriptoManajo
    *
    * Options read from a xml file.
    *
    * @var string
    */
   var $config;
	
   /**
	 * MySql Object
    *
    * Interface to deal with mysql database.
    *
    * @var MySqlData
    */
   var $sql;
	
   /**
	 * Status of user
    *
    * Values are: True or False
    *
    * @var bool
    */
   var $_auth;
	
	// }}}
   // {{{ Script()

	/**
    * Define some dependencies for the object
    *
    * + Reads config file
    * + Starts a MySql valid connection
	 * + Sets Authenticated flag
    *
    * @return void
    *
    * @access public
    * @static
    * @since Constructor available since Release 1.0.0
   */
   function Script($confName)
   {
		//read xml config file
      $xml = new XmlData;
      $xml->read($confName);
      $this->config =& $xml->data;

      //start mysql connection
      $this->sql = new MySQLData($this->config['database']['prefix']);
      $this->sql->connect($this->config['database']['user'], $this->config['database']['pass'], $this->config['database']['name']);
		$_auth = False;
   }
	
	// }}}
   // {{{ _isLocked()
	
	/**
    * Checks if given directory is locked
    *
	 * @param string $dir  directory name
	 *
    * @return bool True if it's locked, otherwise returns False
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
	function _isLocked($dir)
	{
		return file_exists("{$dir}/{$this->config['options']['lock']}"); 
	}
	
	// }}}
	// {{{ unlock()

	/**
    * Unlocks a directory
    *
	 * @param string $dir  directory to lock
	 *
    * @return bool True if unlock, otherwise returns False, 0 if directory is already unlocked
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function unlock($dir)
	{   
		if($this->_isLocked($dir))
		{
			if(unlink("$dir/".$this->config['options']['lock']))
				return True;
			else
				return False;
		} else
		{
			echo '<p>Erro: Diret�rio j� destrancado.</p>';
			return 0;
		}
	}
	
	// }}}
	// {{{ lock()
	
	/**
    * Locks a directory
    *
	 * @param string $dir  directory to lock
	 *
    * @return bool True if lock, otherwise returns False, 0 if directory is already locked
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function lock($dir)
	{   
		if(!$this->_isLocked($dir)):
			$fbuffer = fopen("{$dir}/{$this->config['options']['lock']}", "w");
			if( $fbuffer == 0 )
			{
				echo "N�o foi poss�vel criar o arquivo de tranca.";
				return False;
			}
			fclose($fbuffer);
			return True;
		else:
			echo "<p>Erro: Diret�rio j� trancado.</p>";
			return 0;
		endif;
	}

	// }}}
	// {{{ listLock()
	
	/**
    * Create a form with every directory status
    *
	 * @param string $dir  directory to lock
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function listLock()
	{
		global $base;
		
		//read all directories
		$main = $this->readDirFiles($this->config['options']['root']); 
		array_multisort($main);
		$total = sizeof($main);

		//parse each directory
		for($x = 0; $x < $total; $x++):
		  if(basename($main[$x]) != (".."|".")):
				echo '<form name="editLock" action="'.$base.'?page='.OPT_LOCK.'" method="post">';
				echo '<div class="dirs">Pasta: <strong>'.basename($main[$x]).'</strong> - Status: <input name="lock" type="radio" value="on" ';
				if($this->_isLocked($main[$x]))
					echo "checked";
					
				echo '>Trancada</input> <input name="lock" type="radio" value="off" ';

				if(!$this->_isLocked($main[$x]))
					echo "checked";

				echo '>Destrancada</input><input name="opt" type="hidden" value="lock">';
				echo '<input name="dirName" type="hidden" value="'.$main[$x].'">';
				echo ' <input class="button" name="ok" type="submit" value="Alterar">';
				echo '</div></form>';
			endif;
		endfor;
	}
	
	// }}}
	// {{{ userAdd()

	/**
    * Add a user to the database
    *
	 * @param string $nick  user nickname
	 * @param string $pass  user password
	 * @param string $email  user email
	 * @param int $level  user level
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userAdd($nick, $pass, $email, $level)
   {
		if(!$nick or !$pass or !$email)
			echo "<p>� preciso preencher TODOS os campos</p>";
		else
			$this->sql->addData(DB_USERS, array("nick" => $this->_encodeAll($nick), "pass" => md5(md5($pass)), "email" => $this->_encodeAll($email), "level" => $level));
   }
	
	// }}}
	// {{{ userEdit()
	
	/**
    * Edit user information
    *
	 * @param int $id  user unique ID number
	 * @param string $nick  user nickname
	 * @param string $email  user email
	 * @param string $pass  user password
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userEdit($id, $nick, $email, $pass='')
   {
		$data = array("nick" => $this->_encodeAll($nick), "email" => $this->_encodeAll($email));
		if($pass != ''):
			$data['pass'] = md5(md5($pass));
			if($id == $_COOKIE['uid']) //changing self password
				setcookie('upass', md5($pass), time()+3600*24*365);
		endif;
		$this->sql->setData(DB_USERS, $data, array("id" => $id));
   }
   
	// }}}
	// {{{ userSetLevel()

	/**
    * Assign a new level to user
    *
	 * @param int $id  user unique ID number
	 * @param int $level  new level
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userSetLevel($id, $level)
   {
		$this->sql->setData(DB_USERS, array("level" => $level), array("id" => $id));
   }
	
	// }}}
	// {{{ userGetLevel()

	/**
    * Reads user level
    *
	 * @param int $id  user unique ID number
	 *
    * @return int user level
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function userGetLevel($id='')
   {
		if($id != '')	//given user
			$tmp =& $this->sql->readData(DB_USERS, array("id" => $id), True);
		else	//logged user
			$tmp =& $this->sql->readData(DB_USERS, array("id" => $_COOKIE['uid']), True);
      return $tmp['level'];
   }

	// }}}
	// {{{ userDel()

	/**
    * Removes user from database
    *
	 * @param int $id  user unique ID number
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userDel($id)
   {
		$this->sql->delData(DB_USERS, array("id" => $id));
   }

	// }}}
	// {{{ userGetName
	
	/**
    * Gets the name of user
    *
	 * @param int $id  user unique ID number
	 *
    * @return string  username
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userGetName($id)
   {
      $tmp =& $this->sql->readData(DB_USERS, array("id" => $id), True);
      return $this->_decodeAll($tmp['nick']);
   }
	
	// }}}
	// {{{ userGetID
	
	/**
    * Gets the name of user
    *
	 * @param string $name  username
	 *
    * @return int  user ID
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userGetID($name)
   {
      $tmp =& $this->sql->readData(DB_USERS, array("nick" => $name), True);
		if($tmp == 0)
			return 0;
		else
			return $tmp['id'];
   }
	
	// }}}
	// {{{ userGet()
	
	/**
    * Gets ALL the information of an user
    *
	 * @param int $id  user unique ID number
	 *
    * @return array  all the information of an user
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function userGet($id)
   {
      return $this->sql->readData(DB_USERS, array("id" => $id), True);
   }
	
	// }}}
	// {{{ userGetAll()
	
	/**
    * Gets ALL existent users
	 *
    * @return array  all registered users
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
   function _userGetAll()
   {
		return $this->sql->readData(DB_USERS);
   }
	
	// }}}
	// {{{ userList()
	
	/**
    * Prints all users in a table
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function userList()
	{
		global $base;
		$member = $this->_userGetAll();	
		if($member == 0):
			echo '<p class="center">N�o h� membros</p>';
		else:
			echo '<table class="users"><tr>';
			echo '<td class="id">Id</td>';
			echo '<td class="nick">Nick</td>';
			echo '<td class="nick">Senha</td>';
			echo '<td class="level">Email</td>';
			echo '<td class="misc">A��o</td>';
			echo '</tr>';
	
			foreach($member as $user)
			{
				echo '<tr>';
				echo '<form method="post" action="'.$base."?page=".OPT_USER.'" >';
				echo "<td>{$user["id"]}</td>";
				echo '<td><input class="form" name="nick" type="text" value="'.$user["nick"].'"></td>';
				echo '<td><input class="form" name="pass" type="password" /></td>';
				echo '<td><input class="form" name="email" type="text" value="'.$user["email"].'" />';
				echo '<input name="opt" type="hidden" value="edit"><input name="id" type="hidden" value="'.$user["id"].'"></td>';
				echo '<td><input class="button" type="submit" value="Editar"> <a class="button" href="'.$base."?page=".OPT_USER."&opt=del&id=".$user["id"].'">Apagar</a>&nbsp;<a class="button" href="'.$base."?page=".OPT_USER."&opt=level&id=".$user["id"].'">Alterar N�vel</a></td>';
				echo '</form></tr>';
			}
			echo '</table>';    
		endif;
	}

	// }}}
	// {{{ readDirFiles()
	
	/**
    * Reads all files from a directory
    *
	 * @param string $dir  directory name to read files
	 *
    * @return array all the directories name or False if directory doens't exist
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function readDirFiles($dir)
	{
		$handle = opendir($dir);
			
		if(!$handle): //if directory doesn't exist
			echo "<p>Erro: O diret�rio '{$dir}' n�o existe!</p>";
			return False;
		else:
			while($file = readdir($handle)) :
				//ignore controls
				if (!(($file == ".." or $file == ".") and ($dir == $this->config['options']['root'])) and ($file != ".") ) 
					$filelisting[] = "$dir/$file"; 
			endwhile; 
		endif;

		return $filelisting;
	}
	
	// }}}
	// {{{ listFiles()

	/**
    * List all registered files from a directory for admin control
    *
	 * @param string $dir  directory to search
	 *
    * @return void
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
	function _listFiles($dir)
	{
		global $base;
		$scripts = $this->sql->readData(DB_SCRIPTS, array( "folder" => $dir), False, SQL_NORMAL, "*", " ORDER BY file");
	
		if($scripts == 0):
			echo "<p>N�o h� scripts com status alterado.</p>";
		else:
			echo '<table class="users">';
			echo '<tr>';
			echo '<td class="file">Arquivo</td>';
			echo '<td class="nick">Tradutor</td>';
			echo '<td class="comments">Coment�rio</td>';
			echo '<td class="misc">A��es</td>';
			echo '</tr>';
			foreach($scripts as $file)
			{
				echo '<tr>';
				echo '<form action="'.$base.'?page='.OPT_SCRIPT.'&opt=form'.'" method="post" name="editScript">';
				echo "<td>{$file["file"]}</td>";
				echo '<td>'.$this->userGetName($file["tid"]).'</td>';
				echo '<td class="fullComment">'.substr($this->_decodeAll($file["comment"]), 0, 40).'...</td>';
				echo '<input name="id" type="hidden" value="'.$file["id"].'">';
				echo '<td>';
				echo '<input class="button" name="ok" type="submit" value="Alterar"> ';
				if($this->userGetLevel() == LEVEL_ADM)
					echo '<a href="'.$base.'?page='.OPT_SCRIPT.'&opt=del&id='.$file["id"].'" class="button">Apagar</a>';
				echo '</td></form></tr>';
			}
			echo '</table>';
		endif;
	}
	
	// }}}
	// {{{ admin_ListDirs()

	/**
    * List all registered files from all directories inside @ROOT@
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function admin_ListDirs()
	{
		$dir = $this->readDirFiles($this->config['options']['root']); 
		array_multisort($dir);
		for($x = 0; $x < sizeof($dir); $x++):
			$name = basename($dir[$x]);
			if($name != (".."|".")):	//except control ones
				echo "<h3>$name</h3>";
				$this->_listFiles($name);
			endif;
		endfor;
	}
	
	// }}}
	// {{{ scriptAdd()
	
	/**
    * Add a script to the database
    *
	 * @param string $folder  script's directory
	 * @param string $file  script's name
	 * @param int $owner  owner ID
	 * @param string $comment  comment about
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function scriptAdd($folder, $file, $owner, $comment, $status=STATUS_GOT)
	{
		if($comment == "Sem Coment�rios no momento.")
			$comment = ""; 
		$this->sql->addData(DB_SCRIPTS, array("folder" => $folder, "file" => $file, "tid" => $owner, "version" => "0.0", "rev" => "0", "comment" => $this->_encodeAll($comment), "status" => "$status"));
	}
	
	// }}}
	// {{{ scriptGet()

	/**
    * Get script information
    *
	 * @param int $id  script unique ID
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function scriptGet($id)
   {
		return $this->sql->readData(DB_SCRIPTS, array("id" => $id), True);
   }
	
	// }}}
	// {{{ scriptEdit()
	
	/**
    * Edit script information
    *
	 * @param int $id  script unique ID
	 * @param string $version  script version
	 * @param int $rev  script revision flag
	 * @param string $comment  script comment
	 * @param int $status  script status flag
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function scriptEdit($id, $version, $rev, $comment, $status)
   {
		$this->sql->setData(DB_SCRIPTS, array("version" => $version, "rev" => $rev, "comment" => $this->_encodeAll($comment), "status" => $status), array("id" => $id));
   }
	
	// }}}
	// {{{ scriptDel()

	/**
    * Remove script status
    *
	 * @param int $id  script unique ID
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function scriptDel($id)
	{
		$this->sql->delData(DB_SCRIPTS, array("id" => $id));
	}
	
	// }}}
	// {{{ scriptSearch()

	/**
    * Find if script has any status in database
    *
	 * @param string $name  script name
	 * @param string $folder  script folder
	 *
    * @return array  script information
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
	function _scriptSearch($name, $folder)
	{
		return $this->sql->readData(DB_SCRIPTS, array("file" => $name, "folder" => $folder), True, SQL_AND);
	}
	
	// }}}
	// {{{ getList()

	/**
    * List all files from @ROOT@ directory into a table
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function getList()
	{
		$mainDir = $this->readDirFiles($this->config['options']['root']);
		array_multisort($mainDir);
		$total = sizeof($mainDir);
	
		for($x = 0; $x < $total; $x++): //read all folder data
			if((filetype($mainDir[$x]) == "dir") and (basename($mainDir[$x]) != "..")):	//need to be a folder
				$cdsDir = $this->readDirFiles($mainDir[$x]); 
				array_multisort($cdsDir);
				$dirName = basename($mainDir[$x]);
				$dirTotal = sizeof($cdsDir);
				if($this->_isLocked($mainDir[$x]) == 0):
					echo "<h2><img src=\"./imgs/folder_open.png\" alt=\"Pasta Aberta\" />$dirName: ".($dirTotal - 1)." Arquivos - ".$this->statusOk($dirName, $dirTotal)."% Traduzidos - ".$this->statusRevised($dirName, $dirTotal)."% Revisados</h2>";
					if(($dirTotal - 1) > 0):
						echo '<table>';
						echo '<tr>';
						echo '<td class="script">Scripts</td>';
						echo '<td class="size">Tamanho</td>';
						echo '<td class="author">Respons�vel</td>';
						echo '<td class="version">Vers�o</td>';
						echo '<td class="review">Revisado</td>';
						echo '<td class="comment">Coment�rio</td>';
						echo '</tr>';
		
						//print
						$filesNumber = sizeof($cdsDir);
						for($y = 0; $y < $filesNumber; $y++):
						  //isn't a folder
						  //isn't index.html
						  if((filetype($cdsDir[$y]) != "dir") and (basename($cdsDir[$y]) != "..")  and (basename($cdsDir[$y]) != "index.html"))
								$this->_printFileInfo($cdsDir[$y], ($y+1)%2, basename($mainDir[$x]));
						endfor;
						
						echo"</table>";
					endif;
				else: //locked folder
					echo '<h2><img src="./imgs/folder_locked.png" alt="Pasta Trancada" />'.$dirName.': Trancado.</h2>';
				endif;				
			endif;
		endfor;
	}
	
	// }}}
	// {{{ printFileInfo()

	/**
    * Print info for a file accordly with user status
    *
	 * @param string $name  script name
	 * @param int $type  even or odd line number
	 * @param string $folder  script folder
	 *
    * @return void
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
	function _printFileInfo($name, $type, $folder)
	{   
		global $base;
		$fileInfo = $this->_scriptSearch(basename($name), $folder);
		
		if($fileInfo):	//someone is already with it
			echo "<tr";
			if($type)
				echo ' class="odd"';
			echo "><td>";
			if($fileInfo["status"] == STATUS_LOCK):
				echo '<img src="./imgs/script_lock.png" alt="Script Trancado para tradu��o."/> ';
				echo '<a href="'.$name.'" class="status_locked">';
			elseif($fileInfo["status"] == STATUS_INVALID):
				echo '<img src="./imgs/script_invalid.png" alt="Script Inv�lido para tradu��o."/> ';
				echo '<a href="'.$name.'" class="status_forbiden">';
			elseif(($fileInfo["status"] == STATUS_OK) and ($fileInfo["rev"] == 1)):
				if($this->isloged())
					echo '<a href="'.dirname($name).'/done/'.basename($name).'"> <img src="./imgs/script_done.png" alt="Script Conclu�do."/> </a>';
				else
					echo ' <img src="./imgs/script_done.png" alt="Script Traduzido."/> ';
				echo '<a href="'.$name.'" class="status_done">';
			elseif($fileInfo["status"] == STATUS_OK):
				if($this->isloged())
					echo '<a href="'.dirname($name).'/done/'.basename($name).'"> <img src="./imgs/script_ready.png" alt="Script Traduzido."/> </a>';
				else
					echo '<img src="./imgs/script_ready.png" alt="Script Traduzido."/>';
				echo '<a href="'.$name.'" class="status_translated">';
			else:
				echo '<img src="./imgs/script_edit.png" alt="Script ainda n�o entregue."/> ';
				echo '<a href="'.$name.'" class="status_notdone">';
			endif;
	
			echo $fileInfo["file"].'</a></td>
				<td>'.filesize($name).'b</td>';
				if(($fileInfo["status"] == STATUS_LOCK) or ($fileInfo["status"] == STATUS_INVALID))
					echo '<td>Sistema</td>';
				else
					echo '<td>'.$this->userGetName($fileInfo["tid"]).'</td>';

				echo '<td>'.$fileInfo["version"].'</td>';
				echo '<td><img src="./imgs/';;
		
			if(!$fileInfo["rev"])
				echo 'not';
	
			echo 'revised.png" /></td>';
			if($fileInfo["status"] == STATUS_LOCK)
				echo '<td class="status_forbiden">Script trancado para tradu��o.</td>';
			elseif($fileInfo["status"] == STATUS_INVALID)
				echo '<td class="status_forbiden">Script inv�lido para tradu��o.</td>';
			else
				echo '<td class="fullComment">'.(strlen($fileInfo["comment"]) > 50 ? substr($this->_decodeAll($fileInfo["comment"]), 0, 45)."(...)" : ( strlen($fileInfo["comment"]) == 0 ? "..." : $this->_decodeAll($fileInfo["comment"])) ).'</td>';
			echo "</tr>";
			
		elseif($this->isloged() and ($this->userGetLevel() != LEVEL_OFF)):   //script free & user logged
			echo '<form action="'.$base.'?page='.OPT_GET.'" method="POST" name="ScriptOwn">';
			
			echo "<tr";
			
			if($type)
				echo ' class="odd"';
				
			echo ">";			
			echo '<td><img src="./imgs/script_open.png" alt=""/><a href="'.$name.'">'.basename($name).'</a></td>';
			echo "<td>".filesize($name)."b</td>";
			echo "<td>Sem Dono</td>";
			echo '<input name="tid" type="hidden" value="'.$_COOKIE['uid'].'" />';
			echo "<td>--</td>";
			echo '<td><img src="./imgs/notrevised.png" alt="Script N�o Revisado"/></td>';
			echo '<td><input class="form" name="comment" type="text" value="Sem Coment�rios no momento." onfocus="if (this.value == \'Sem Coment�rios no momento.\') this.value = \'\';" size="30" maxlength="254" /> &nbsp; <input name="ok" class="button" type="submit" value="Pegar Script" /></td>';
			echo '<input name="file" type="hidden" value="'.basename($name).'" />';
			echo '<input name="folder" type="hidden" value="'.$folder.'" />';
			echo "</tr>";
			echo '</form>';

		else: //Script free, but user not logged
			echo "<tr";
			if($type)
				echo ' class="odd"';

			echo ">";
			echo '<td><img src="./imgs/script_open.png" alt="Script sem dono."/> <a href="'.$name.'">'.basename($name).'</a></td>
					<td>'.filesize($name).'b</td>
					<td>--</td>
					<td>--</td>
					<td><img src="./imgs/notrevised.png" alt="Script N�o Revisado"/></td>
					<td>--</td>
					</tr>';
		endif;
	}
	
	// }}}
	// {{{ _encodeAll()

	/**
    * Encode a string to not have problems
    *
	 * @param string $data  string data to encode
	 *
    * @return string  encoded string
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
   function _encodeAll($data)
   {
      return htmlentities(addslashes($data));
   }
	
	// }}}
	// {{{ _decodeAll()
 
	/**
    * Decode a string to normal form
    *
	 * @param string $data  string data to decode
	 *
    * @return string  decoded string
    *
    * @access private
    * @static
    * @since Method available since Release 1.0.0
   */
   function _decodeAll($data)
   {
      return stripslashes($data);
   }
	
	// }}}
	// {{{ getStatus()

	/**
    * Get general status
    *
	 * @param string $folder  folder to process
	 * @param int $total  total files
	 * @param int $percent  percentage flag
	 *
    * @return string  status string
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
	function statusOk($folder, $total, $percent=True)
	{
		$num = $this->sql->readData(DB_SCRIPTS, array("status" => STATUS_OK, "folder" => $folder), True, SQL_AND, "COUNT(*)");
		if($percent)
			return substr(($num["COUNT(*)"]/$total)*100, 0, 4);
		else
			return $num["COUNT(*)"];
	}
	
	// }}}
	// {{{ statusRevised
	
	function statusRevised($folder, $total, $percent=True)
	{
		$num = $this->sql->readData(DB_SCRIPTS, array("status" => STATUS_OK, "folder" => $folder, "rev" => 1), True, SQL_AND, "COUNT(*)");
		if($percent)
			return substr(($num["COUNT(*)"]/$total)*100, 0, 4);
		else
			return $num["COUNT(*)"];
	}
	
	// }}}
	// {{{ login
	
	/**
    * User login
    *
	 * @param string $user  username
	 * @param string $pass  user password
	 *
    * @return bool  True if username and password match, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function login($user, $pass)
   {
		$tmp = $this->sql->readData(DB_USERS, array("nick" => $user, "pass" => md5(md5($pass))), True, SQL_AND);
      if($tmp != 0):
			setcookie('uid', $tmp["id"], time()+3600*24*365);
			setcookie('upass', md5($pass), time()+3600*24*365);
			$this->_auth = True;
			return True;
      else:
			return False;
      endif;
   }
	
	// }}}
	// {{{ isLoged()

	/**
    * Check if user is logged
	 *
    * @return bool  True if logged, otherwise returns False
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function isloged()
   {
		if(isset($_COOKIE['uid']) and isset($_COOKIE['upass'])):
			$tmp = $this->sql->readData(DB_USERS, array("id" => $_COOKIE['uid']), True);
			if($tmp["pass"] == md5($_COOKIE['upass']))
				return True;
			else
				return False;
		else:
			return False;
		endif;
   }
	
	// }}}
	// {{{ logout()

	/**
    * logout user
	 *
    * @return void
    *
    * @access public
    * @static
    * @since Method available since Release 1.0.0
   */
   function logout()
   {
		setcookie("uid", '', time()-3600*24*365);
		setcookie('upass', '', time()+3600*24*365);
		$this->_auth = False;
   }
	
	// }}}
	// {{{ whois()

	function whois()
	{
		$tmp = $this->sql->readData("users", array("id" => $_COOKIE['uid']), True);
		return $tmp["nick"];
	}
	
	// }}}
	// {{{ getTitle()

	function getTitle()
	{
		return $this->config['options']['project'];
	}
	
	// }}}
	// {{{ getIcon()

	function getIcon()
	{
		return $this->config['options']['icon'];
	}
	
	// }}}
}

// }}}
?>