<?php
    include_once("./ScriptoManajo.php");
	 $script = new Script("./config.xml");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt_BR">
   <head>    
      <title><?php echo $script->getTitle() ?> - Gerenciador de Scripts</title>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <link rel="stylesheet" type="text/css" href="./main.css" />
	   <link rel="shortcut icon" href="<?php echo $script->getIcon()?>" type="image/x-icon" />
		<!--[if lt IE 7]>
			<script defer type="text/javascript" src="lib/pngfix.js"></script>
			<link rel="stylesheet" type="text/css" media="screen" href="ie.css" />
		<![endif]-->
   </head>
   <body>
		<div id="header">       
			<span>[FallOut Version]</span>
		</div>
		<ul id="menu">
			<?php
				if($script->isloged()):
					echo '<li>Ol� '.$script->whois().'</li>';
					if(($script->userGetLevel() == LEVEL_ADM) or ($script->userGetLevel() == LEVEL_REV) or ($script->userGetLevel() == LEVEL_BTH))
						echo '<li><a title="Administra��o Geral." href="./admin.php">Admin</a></li>';
					echo '<li><a href="./auth.php?logout=1">LogOut</a></li>';
				else:
					echo '<li><a title="Sobre o ScriptoManajo." href="'.$base."?page=".MENU_LOGN.'">Login</a></li>';
				endif;
			?>
			<!--<li><a title="Informa��es gerais." href="<?php echo($base."?page=".MENU_INFO); ?>">Informa��es</a></li>-->
			<li><a title="Ir � lista principal de scripts dispon�vel." href="<?php echo($base); ?>">Scripts</a></li>
	
			<li><a title="Uma lista de projetos existentes nesta se��o." href="<?php echo($base."?page=".MENU_PROJ); ?>">Projetos</a></li>
 		   <li><a title="Entre em contato com a equipe." href="<?php echo($base."?page=".MENU_CONT); ?>">Contato</a></li>
			<li><a title="Sobre o ScriptoManajo." href="<?php echo($base."?page=".MENU_ABOT); ?>">Sobre</a></li>
		</ul>
		
		<div id="main">
      <?php
         //Main Menu
         if(!isset($_REQUEST["page"])):
				$script->getList();
         //honeypot
         elseif(strstr($_REQUEST["page"], "http://") or strstr($_REQUEST["page"], "www.")):
            require_once("./lamehunter.php");
			//emptylogin message
         elseif($_REQUEST["page"] == MSG_EMPTYLOGIN):
			   echo '<h2>Erro</h2><p>� preciso preeencher TODOS os campos para poder fazer login.</p>';
			//wrong login message
         elseif($_REQUEST["page"] == MSG_WRONGLOGIN):
				echo '<h2>Erro</h2><p>Nome ou Senha errados. Por favor corrija e tente novamente.</p>'; 
         //login page
         elseif($_REQUEST["page"] == MENU_LOGN):
				echo '<h2 class="login">Se��o de login:</h2>
						<form action="./auth.php" method="post">
							<p>Usu�rio: <input class="form" type="text" name="user" size="10" /></p>
							<p>Senha: <input class="form" type="password" name="pass" size="10" /><input type="hidden" name="login" value="1" /></p>
							<p><input type="submit" class="button" value="Enviar" /></p>
						</form>';
			//info page
         /*elseif($_REQUEST["page"] == MENU_INFO):
			 *	echo '<h2><span class="hilight">'.$script->getTitle().'</span> - P�gina de informa��es</h2>';
			 */
			//user getting a script
         elseif($_REQUEST["page"] == OPT_GET):
				echo '<h2>Pegando arquivo:</h2>';
				$script->scriptAdd($_POST["folder"], $_POST["file"], $_POST["tid"], $_POST["comment"]);
				echo '<p>Voc� ser� redirecionado automaticamente em <strong>5</strong> segundos.</p>';
				echo '<p><a href="./index.php" >Se n�o quizer esperar voc� poder� clicar aqui para ir � p�gina principal imediatamente.</a></p>';
				echo '<meta http-equiv="refresh" content="5;url=./index.php">';
         //About page
         elseif($_REQUEST["page"] == MENU_ABOT):
            require_once("./about.php"); 
         //Contact page
         elseif($_REQUEST["page"] == MENU_CONT):
            require_once("./contact.php"); 
         //Ofline page
			elseif($_REQUEST["page"] == MENU_OFF):
             require_once("./off.php"); 
         //User lost?
         else:
            require_once("./numb.php");     
			endif;
      ?>
		</div>

		<div id="iconsStart">
			<img src="./imgs/script_open.png" alt="Script livre para tradu��o" /> Script Normal |
			<img src="./imgs/script_edit.png" alt="Script em tradu��o" /> <span class="status_notdone">Script em tradu��o</span> |
			<img src="./imgs/script_ready.png" alt="Script Entregue" /> <span class="status_translated">Script entregue</span> |
			<img src="./imgs/script_done.png" alt="Script Pronto" /> <span class="status_done">Script pronto</span> |
			<img src="./imgs/script_lock.png" alt="Script trancado para revis�o" /> <span class="status_locked">Script trancado</span> |
			<img src="./imgs/script_invalid.png" alt="Script inv�lido para tradu��o" /> <span class="status_forbiden">Script inv�lido</span>
		</div>
		<div id="iconsEnd">
	 	   <img src="./imgs/revised.png" alt="Script Revisado" /> Script Revisado |
	 	   <img src="./imgs/notrevised.png" alt="Script N�o Revisado" /> Script N�o Revisado |
		   <img src="./imgs/folder_open.png" alt="Pasta com scripts liberados para tradu��o" /> Pasta Liberada |
		   <img src="./imgs/folder_locked.png" alt="Pasta trancada para tradu��o" /> Pasta Trancada
		</div>

		<div id="foot"><a href="http://trans-center.org">Trans-Center</a> - Todos os Direitos Reservados.</div>
   </body>
</html>